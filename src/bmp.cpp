#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "E:\random\cppExpirements\bmp.h"


//Read
BMP::BMP(const char * fname){
    read(fname);
}

void BMP::read(const char * fname){
    std::ifstream ifile(fname, std::ios_base::binary);
    if (ifile){
        ifile.read((char*)&file_header, sizeof(file_header));
        std::cout << "Size of file_header: " << sizeof(file_header) << std::endl;
        std::cout << "Size of info_header: " << sizeof(info_header) << std::endl;
        if(file_header.file_type!=0x4D42)
            throw std::runtime_error("Error! File format not recognized");

        ifile.read((char*)&info_header, sizeof(info_header));
        //Color Header only used for transparent images
        if(info_header.bit_count == 32){
            //Check if the file ahs bit mask color information
            if(info_header.size>= (sizeof(info_header) + sizeof(color_header)))
            {
                ifile.read((char*)&color_header, sizeof(color_header));
                //Check to see if pixel data is stored as BGRA and if the color space type is sRGB
                check_color_header(color_header);
            }
            else
            {
                std::cerr << "Waring! The file \"" << fname << "\" does not seem to continue" << std::endl;
                throw std::runtime_error("Error! Unrecognized file format");
            }
        }
        //Jump to pixel data location
        ifile.seekg(file_header.pixel_data_offset, ifile.beg);

        //Adjust the header fields for output
        //Some editors will put extra info in image file, only save the headers and the data
        if(info_header.bit_count==32){
            info_header.size = sizeof(info_header) + sizeof(color_header);
            file_header.pixel_data_offset = sizeof(file_header) + sizeof(info_header) + sizeof(color_header);
        }else{
            info_header.size = sizeof(info_header);
            file_header.pixel_data_offset = sizeof(file_header) + sizeof(info_header);
        }
        file_header.file_size = file_header.pixel_data_offset;
        
        if(info_header.height<0){
            throw std::runtime_error("This program can treat only BMP images with the origin in the bottom left corner");
        }

        data.resize(info_header.width * info_header.height * info_header.bit_count / 8);

        //Check if need to take into account row padding
        if (info_header.width % 4 == 0){
            ifile.read((char*)data.data(),data.size());
            file_header.file_size += data.size();
        }
        else {
            row_stride = info_header.width * info_header.bit_count / 8;
            uint32_t new_stride = make_stride_aligned(4);
            std::vector<uint8_t> padding_row(new_stride - row_stride);

            for(int y = 0; y < info_header.height; ++y){
                ifile.read((char*)(data.data() + row_stride * y), row_stride);
                ifile.read((char*)padding_row.data(), padding_row.size());
            }
            file_header.file_size += data.size() + info_header.height * padding_row.size();
        }
    }
    else {
        throw std::runtime_error("Unable to open the input image file.");
    }
}

//Write
BMP::BMP(int32_t width, int32_t height, bool has_alpha = true) {

    if (width <= 0 || height <= 0) {
        throw std::runtime_error("The image width and height must be positive numbers.");
    }

    info_header.width = width;
    info_header.height = height;
    if(has_alpha){
        info_header.size = sizeof(info_header) + sizeof(color_header);
        file_header.pixel_data_offset = sizeof(file_header) + sizeof(info_header) + sizeof(color_header);

        info_header.bit_count = 32;
        info_header.compression = 3;
        row_stride = width * 4;
        data.resize(row_stride*height);
        file_header.file_size = file_header.pixel_data_offset + data.size();
    }else{
        info_header.size = sizeof(info_header);
        file_header.pixel_data_offset = sizeof(file_header) + sizeof(info_header);

        info_header.bit_count = 24;
        info_header.compression = 0;
        row_stride = width * 3;
        data.resize(row_stride * height);
        
        uint32_t new_stride = make_stride_aligned(4);
        file_header.file_size = file_header.pixel_data_offset + data.size() + info_header.height * (new_stride - row_stride);
    }
}

void BMP::write(const char * fname){
    std::cout << "=====FILE INFO=====" << std::endl;
    std::cout << "File type:" << file_header.file_type << std::endl;
    std::cout << "File size:" << file_header.file_size << std::endl;
    std::cout << "Offset:" << file_header.pixel_data_offset << std::endl;

    std::cout << "Header size:" << info_header.size << std::endl;
    std::cout << "Width:" << info_header.width << std::endl;
    std::cout << "Height:" << info_header.height << std::endl;

    std::cout << "Color Planes:" << info_header.planes << std::endl;
    std::cout << "Bit Count:" << info_header.bit_count << std::endl;
    std::cout << "Compression:" << info_header.compression << std::endl;
    std::cout << "Image size:" << info_header.size_image << std::endl;
    std::cout << "Colors used:" << info_header.color_used << std::endl;
    std::ofstream ofile(fname, std::ios_base::binary);
    if(ofile){
        if(info_header.bit_count==32){
            write_headers_and_data(ofile);
        }
        else if(info_header.bit_count==24){
            if(info_header.width % 4 == 0){
                write_headers_and_data(ofile);
            }
            else {
                uint32_t new_stride = make_stride_aligned(4);
                std::vector<uint8_t> padding_row(new_stride - row_stride);

                write_headers(ofile);

                for (int y = 0; y < info_header.height; ++y){
                    ofile.write((const char*)(data.data() + row_stride * y), row_stride);
                    ofile.write((const char*)padding_row.data(), padding_row.size());
                }
            }
        }
        else {
            throw std::runtime_error("The program can treat only 24 or 32 bits per pixel BMP files");
        }    
    }
    else{
        std::runtime_error("Unable to open the output image file.");
    }

}

void BMP::fill_region(uint32_t x0, uint32_t y0, uint32_t w, uint32_t h, uint8_t B, uint8_t G, uint8_t R, uint8_t A) {
    if (x0 + w > (uint32_t)info_header.width || y0 + h > (uint32_t)info_header.height) {
        throw std::runtime_error("The region does not fit in the image!");
    }

    uint32_t channels = info_header.bit_count / 8;
    for (uint32_t y = y0; y < y0 + h; ++y) {
        for (uint32_t x = x0; x < x0 + w; ++x) {
            data[channels * (y * info_header.width + x) + 0] = B;
            data[channels * (y * info_header.width + x) + 1] = G;
            data[channels * (y * info_header.width + x) + 2] = R;
            if (channels == 4) {
                data[channels * (y * info_header.width + x) + 3] = A;
            }
        }
    }
}

void BMP::monochrome(){
    uint32_t channels = info_header.bit_count / 8;
    for(uint32_t y = 0; y < info_header.height; ++y){
        for(uint32_t x = 0; x < info_header.width; ++x){
            uint8_t B = data[channels * (y * info_header.width + x) + 0];
            uint8_t G = data[channels * (y * info_header.width + x) + 1];
            uint8_t R = data[channels * (y * info_header.width + x) + 2];

            //average value
            // float mag = B + G + R;
            // uint8_t magnitudeV = mag/3;
            // B=magnitudeV;
            // G=magnitudeV;
            // R=magnitudeV;

            //Grayscale scaling factor
            B=B*0.0722;
            G=G*0.7152;
            R=R*0.2126;

            data[channels * (y * info_header.width + x) + 0] = B;
            data[channels * (y * info_header.width + x) + 1] = G;
            data[channels * (y * info_header.width + x) + 2] = R;
        }
    }
}

void BMP::eighthGradeHandout(){
    uint32_t channels = info_header.bit_count / 8;
    for(uint32_t y = 0; y < info_header.height; ++y){
        for(uint32_t x = 0; x < info_header.width; ++x){
            uint8_t B = data[channels * (y * info_header.width + x) + 0];
            uint8_t G = data[channels * (y * info_header.width + x) + 1];
            uint8_t R = data[channels * (y * info_header.width + x) + 2];

            //average value
            float mag = B + G + R;
            uint8_t magnitudeV = mag/3;

            //8th grade teacher's copying machine
            if(magnitudeV > 180){
            B=magnitudeV;
            G=magnitudeV;
            R=magnitudeV;
            }
            else{
            B=0;
            G=0;
            R=0;
            }

            data[channels * (y * info_header.width + x) + 0] = B;
            data[channels * (y * info_header.width + x) + 1] = G;
            data[channels * (y * info_header.width + x) + 2] = R;
        }
    }

}

void BMP::rainbow(){
    uint32_t channels = info_header.bit_count / 8;
    for(uint32_t y = 0; y < info_header.height; ++y){
        for(uint32_t x = 0; x < info_header.width; ++x){
            uint8_t B = data[channels * (y * info_header.width + x) + 0];
            uint8_t G = data[channels * (y * info_header.width + x) + 1];
            uint8_t R = data[channels * (y * info_header.width + x) + 2];
            
            //rainbow :)
            B=G * 2;
            G=R * 2;
            R=B * 2;

            data[channels * (y * info_header.width + x) + 0] = B;
            data[channels * (y * info_header.width + x) + 1] = G;
            data[channels * (y * info_header.width + x) + 2] = R;
        }
    }

}

void BMP::invertColor(){
    uint32_t channels = info_header.bit_count / 8;
    for(uint32_t y = 0; y < info_header.height; ++y){
        for(uint32_t x = 0; x < info_header.width; ++x){
            uint8_t B = data[channels * (y * info_header.width + x) + 0];
            uint8_t G = data[channels * (y * info_header.width + x) + 1];
            uint8_t R = data[channels * (y * info_header.width + x) + 2];
            
            //Invert Colors
            B=255-B;
            G=255-G;
            R=255-R;

            data[channels * (y * info_header.width + x) + 0] = B;
            data[channels * (y * info_header.width + x) + 1] = G;
            data[channels * (y * info_header.width + x) + 2] = R;
        }
    }

}

void BMP::epic(){
    uint32_t channels = info_header.bit_count / 8;
    for(uint32_t y = 0; y < info_header.height; ++y){
        for(uint32_t x = 0; x < info_header.width; ++x){
            uint8_t B = data[channels * (y * info_header.width + x) + 0];
            uint8_t G = data[channels * (y * info_header.width + x) + 1];
            uint8_t R = data[channels * (y * info_header.width + x) + 2];

            int cutoff = 150;
            if(R > cutoff){
                R = 255;
            }
            else{
                R = 0;
            }
            if(B > cutoff){
                B = 255;
            }
            else{
                B = 0;
            }
            if(G > cutoff){
                G = 255;
            }
            else{
                G = 0;
            }

            data[channels * (y * info_header.width + x) + 0] = B;
            data[channels * (y * info_header.width + x) + 1] = G;
            data[channels * (y * info_header.width + x) + 2] = R;
        }
    }

}

void BMP::check_color_header(BMP_Color_Header &color_header){
    BMP_Color_Header expected_color_header;
    if(expected_color_header.red_mask != color_header.red_mask ||
    expected_color_header.green_mask != color_header.green_mask ||
    expected_color_header.blue_mask != color_header.blue_mask ||
    expected_color_header.alpha_mask != color_header.alpha_mask){
        throw std::runtime_error("Unexpected color mask format! The program expects pixels tobe in BGRA format");
    }
    if(expected_color_header.color_space_type != color_header.color_space_type){
        throw std::runtime_error("Unexpected color space type! The program expects sRGB values");
    }
}

void BMP::write_headers(std::ofstream &ofile) {
    ofile.write((const char*) &file_header, sizeof(file_header));
    ofile.write((const char*) &info_header, sizeof(info_header));
    if(info_header.bit_count == 32){
        ofile.write((const char*) &color_header, sizeof(color_header));
    }
}

void BMP::write_headers_and_data(std::ofstream &ofile) {
    write_headers(ofile);
    ofile.write((const char*) data.data(), data.size());
}

uint32_t BMP::make_stride_aligned(uint32_t align_stride){
    uint32_t new_stride = row_stride;
    while (new_stride % align_stride != 0){
        new_stride++;
    }
    return new_stride;
}




int main(int argc, char * argv []){

    std::cout << "*********TestPic*********" << std::endl;
    BMP testpic(argv[1]);   
    testpic.teachersCopyingMachine(); 
    testpic.write(argv[2]);

    //  // Create a BMP image in memory, modify it, save it on disk
    // std::cout << "*********bmp2*********" << std::endl;
    //  BMP bmp2(800, 600);
    //  bmp2.fill_region(0, 0, 100, 200, 0, 0, 255, 255);
    //  bmp2.write("img_test.bmp");
 
    //  // Create a 24 bits/pixel BMP image in memory, modify it, save it on disk
    // std::cout << "*********bmp3*********" << std::endl;
    //  BMP bmp3(203, 217, false);
    //  bmp3.fill_region(0, 0, 100, 100, 255, 0, 255, 255);
    //  bmp3.write("img_test_24bits.bmp");



    return 0;
}
