#ifndef __BMP_H
#define __BMP_H

#include <iostream>
#include <vector>

#pragma pack(push,1)
struct BMP_File_Header 
{
    uint16_t file_type{0x4d42};
    uint32_t file_size{0};
    uint16_t reserved1{0};
    uint16_t reserved2{0};
    uint32_t pixel_data_offset{0};
};
struct BMP_Info_Header
{
    uint32_t size{0};
    uint32_t width{0};
    uint32_t height{0};

    uint16_t planes{1};
    uint16_t bit_count{0};
    uint32_t compression{0};
    uint32_t size_image{0};
    int32_t x_pix_per_meter{0};
    int32_t y_pix_per_meter{0};
    uint32_t color_used{0};
    uint32_t colors_important{0};
};
struct BMP_Color_Header
{
    uint32_t red_mask   {0x00ff0000};
    uint32_t green_mask {0x0000ff00};
    uint32_t blue_mask  {0x000000ff};
    uint32_t alpha_mask {0xff000000};
    uint32_t color_space_type {0x73524742};
    uint32_t unused[16]{0};
};

struct BMP
{
    BMP_File_Header file_header;
    BMP_Info_Header info_header;
    BMP_Color_Header color_header;
    std::vector<uint8_t> data;

    //Read
    BMP(const char * fname);
    void read(const char * fname);
    //Write
    BMP(int32_t width, int32_t height, bool has_alpha);
    void write(const char * fname);

    void fill_region(uint32_t x0, uint32_t y0, uint32_t w, uint32_t h, uint8_t B, uint8_t G, uint8_t R, uint8_t A) ;

    void monochrome();

    void eighthGradeHandout();

    void rainbow();

    void invertColor();

    void epic();

private:
    //Check if the pixel data is stored as BGRA and if the color space type is sRGB
    void check_color_header(BMP_Color_Header &color_header);

    uint32_t row_stride{ 0 };

    void write_headers(std::ofstream &ofile);

    void write_headers_and_data(std::ofstream &ofile);

    //Add 1 to the row_stride until it is divisible with align_stride
    uint32_t make_stride_aligned(uint32_t align_stride);
};
#pragma pack(pop) //compiler can go back to normal packing scheme

#endif
